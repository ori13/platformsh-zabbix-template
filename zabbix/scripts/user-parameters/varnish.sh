#!/bin/bash

function query() {
    STAT_KEY=$1
    VARNISH_STATS_APP=varnish-stats

    # Get current value
    VARNISH_STATS_URL=$(platform relations -A "$VARNISH_STATS_APP" --property varnish.0.url)
    STAT_VALUE_CURRENT=$(platform ssh -A "$VARNISH_STATS_APP" "curl ${VARNISH_STATS_URL}/stats 2>/dev/null" 2>/dev/null | jq -r ".\"MAIN.${STAT_KEY}\".value")

    # Get previous value
    LAST_STAT_VALUE_FILE=/tmp/varnish.stats.${STAT_KEY}
    if [ ! -f "${LAST_STAT_VALUE_FILE}" ]; then
        touch "$LAST_STAT_VALUE_FILE"
        echo 0 > "$LAST_STAT_VALUE_FILE"
    fi
    STAT_VALUE_LAST=$(cat "$LAST_STAT_VALUE_FILE")
    if [ -z "$STAT_VALUE_LAST" ]; then
        STAT_VALUE_LAST=0
    fi

    # Get the current value and update the previous one
    STAT_DELTA=$(expr "$STAT_VALUE_CURRENT" - "$STAT_VALUE_LAST")
    echo "$STAT_VALUE_CURRENT" > "$LAST_STAT_VALUE_FILE"

    # Output the delta of the value
    echo "$STAT_DELTA"
}

if [ $# == 0 ]; then
    echo $"Usage $0 {client_req|cache_hit|cache_miss|cache_hitpass}"
    exit
else
    query "$1"
fi